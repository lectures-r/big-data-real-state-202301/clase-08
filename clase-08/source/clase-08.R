## Eduard Martinez
## update: 17-05-2023

## clean environment
rm(list=ls())

##================================##
## **[0.] Configuración inicial** ##
##================================##

## **0.0 Instalar/llamar las librerías de la clase**
require(pacman) 
p_load(tidyverse,rio,janitor,
       caret, glmnet, elasticnet,ranger,
       yardstick, ## metric_set
       tune, ## tune_grid, select_best
       rsample, ## vfold_cv
       dials, ## grid_regular
       rattle, ## fancyRpartPlot
       parsnip,tree,rpart,rpart.plot) ## trees

### **0.1 fijar semilla**
set.seed(12345)

### **0.2 leer datos**
db <- import("input/data_regresiones.rds") %>%
      select(-id,-l5) %>% na.omit() %>% clean_names() %>% 
      subset(property_type %in% c("Apartamento","Casa"))

### **0.3 prepare db**
df <- model.matrix(~.+ I(surface_total^2) + I(dist_cbd^2) -1 -price , data=db) %>% as_tibble() %>% clean_names()

df$price <- db$price/1000000 %>% unlist()

### **0.4 partir la muestra**
sample_test <- sample(x = nrow(df) , size = nrow(df)*.3)

test <- df[sample_test,]

train <- df[-sample_test,]

##==================##
## **[1.] RECAP** ##
##==================##

### **1.1 Lasso**

## predictores
x <- scale(x=select(train,-price) , center = T , scale = T) %>% as.matrix()
y <- train$price

## estimations
lasso <- glmnet(x=x , y=y , alpha=1) ## (alpha = 1 para lasso)
dim(coef(lasso)) ## Número de variables y modelos
plot(x=lasso, xvar="lambda" , label=TRUE) ## plot

## mejor lambda (CV)
lasso_cv <- cv.glmnet(x = x , y = y , alpha=1 , nfolds = 10)

min_lambda = lasso_cv$lambda.min

## hacer predicción con mejor lambda
coef(lasso)[,which(lasso$lambda==min_lambda)]

test_scale <- scale(x = select(test,-price) , center = T , scale = T) %>% as_tibble()

test_scale$price_lasso_cv <- predict(object=lasso , s=min_lambda , newx=test_scale %>% as.matrix()) %>% unlist()

test_scale$price <- test$price

rmse_lasso = sqrt(with(test_scale,mean((price-price_lasso_cv)^2)))

rmse_lasso

##==================##
## **[2.] Arboles** ##
##==================##

## **2.2 Arboles**

## definir modelo
set_1 <- decision_tree() %>% 
## rpart: ajusta un modelo como un conjunto de declaraciones si/entonces que crea una estructura basada en un árbol.
## For this engine, there are multiple modes: classification, regression, and censored regression
         set_engine("rpart") %>% 
         set_mode("regression") ## modelo de regresion

## ajustar primer modelo:
model_1 <- fit(object = set_1 , formula = price ~ . , data = train)

model_1

fancyRpartPlot(model = model_1$fit)

rpart.plot(x = model_1$fit)

## predicción
test$price_tree <- predict(object=model_1 , new_data=test) %>% unlist()

rmse_tree = sqrt(with(test,mean((price-price_tree)^2)))

rmse_tree

## **2.3 Arboles: Tune**

## definir modelo
set_2 <- decision_tree(cost_complexity = tune(), ## 
                       tree_depth = tune() , ## profundidad
                       min_n = tune() ## minima cantidad de observaciones por hoja
                       ) %>%  
         set_engine("rpart") %>% set_mode("regression")

## crear grilla con hiperparametos a probar
grid_regular(cost_complexity(), 
             min_n(),
             tree_depth(), 
             levels = 4)

tree_grid <- crossing(cost_complexity = c(0.0001), 
                      min_n = c(2,14,27),
                      tree_depth = c(2,4,8))

## Definimos CV
folds <- vfold_cv(data=train , v=5) ## resamples

## ajustar segundo modelo: 
model_2 <- tune_grid(set_2,
                     price ~ .,
                     resamples = folds,
                     grid = tree_grid,
                     metrics = metric_set(rmse))

## ver los hiperparámetros
collect_metrics(model_2)

## visualizar los modelos
autoplot(model_2) + theme_test()

## best model
select_best(model_2)

best_model_2 <- finalize_model(set_2, select_best(model_2))

## Entrenamos el mejor modelo
model_2_fit <- fit(object = best_model_2 , formula = price ~ . , data = train)

## plot
#rpart.plot(x = model_2_fit$fit)

## predicción
test$price_tree_tn <- predict(object=model_2_fit , new_data=test) %>% unlist()

rmse_tree_tn = sqrt(with(test,mean((price-price_tree_tn)^2)))

rmse_lasso
rmse_tree
rmse_tree_tn
  
##==================##
## **[3.] Bosques** ##
##==================##

## modelo 1
rf_1 <- train(price ~ .,
              data = train[1:1000,], 
              method = "ranger", ## ramdon forest
              trControl = trainControl(number=5 , method="repeatedcv"))
rf_1

## plot
plot(rf_1)

## variance: La reducción de la varianza es un método para dividir el nodo que se utiliza cuando la variable objetivo es continua, es decir, problemas de regresión. Se llama así porque usa la varianza como una medida para decidir la función en la que un nodo se divide en nodos secundarios.
## extremely randomized trees: La idea detrás de esto es aumentar la aleatoriedad en la construcción de los árboles, 
 
### 3.3 Tunear un bosque

## 1. Fijar numero de predictores y criterio de parada y elegir numero de arboles
## 2. Fijar numero de arboles y criterio de parada y elegir el numero de predictores
## 3. Fijar numero de predictores y numero de arboles y elegir el criterio de parada

## Creamos una grilla para tunear el random forest
tunegrid_rf <- expand.grid(mtry = c(2,4,8), 
                           min.node.size = c(2,5,10),
                           splitrule = "variance")

## ranger me permite optimizar mas hiperparametros
## fit model
rf_2 <- train(price ~ .,
              data = train[1:1000,], 
              method = "ranger", 
              tuneGrid = tunegrid_rf)
rf_2

# plot
plot(rf_2)

## Ahora vamos a ver como cambian los resultados cuando cambio el número de árboles
resultados <- tibble()
for (t in seq(1, 100, 10)) {
     modelo_i <- ranger(price ~ ., 
                        data = train, 
                        num.trees = t, ## numero de arboles
                        mtry = 8, ## Número de variables para dividir posiblemente en cada nodo.
                        min.node.size = 10, ## Tamaño mínimo de nodo
                        splitrule = "variance") ## Regla de división
    
     rmse_in = sqrt(modelo_i[["prediction.error"]])
     test$price_rf_i <- predict(modelo_i , data=test)$predictions
     rmse_out <- sqrt(with(test,mean((price-price_rf_i)^2)))
    
     result_i <- tibble(arboles = t,rmse_in = rmse_in , rmse_out = rmse_out)
     resultados <- rbind(resultados, result_i)
}
resultados

ggplot() +
geom_line(data=resultados , aes(x=arboles , y=rmse_out) , col="red") + 
geom_line(data=resultados , aes(x=arboles , y=rmse_in) , col="blue") + 
geom_point() + theme_bw() +
labs(x = "Número de árboles", y = "RMSE",
     title = "Variación en el desempeño del Random Forest para diferente número de árboles")


