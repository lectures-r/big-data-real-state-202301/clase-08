---
pagetitle: "Clase 08: Arboles de Clasificación  y Regresión"
title: "**Big Data and Machine Learning en el Mercado Inmobiliario**"
subtitle: "**Clase 08:** Arboles de Clasificación  y Regresión"
author: 
  name: Eduard Fernando Martínez González
  affiliation: Universidad de los Andes | [BD-ML](https://ignaciomsarmiento.github.io/teaching/BDML)
output: 
  html_document:
    theme: flatly
    highlight: haddock
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(knitr,tidyverse,fontawesome,kableExtra)
# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!---------------------->
## **[1.] Introducción**

Los árboles de clasificación y regresión son modelos predictivos formados por reglas binarias (sí/no) o con las que se consigue repartir las observaciones en función de sus atributos y predecir así el valor de la variable respuesta.

![Tomado de: https://i.pinimg.com](https://i.pinimg.com/564x/30/a1/2a/30a12a836d1e3b0b3e6d8e831f2e05cc.jpg){width=60%}

Muchos métodos predictivos generan modelos globales en los que una única ecuación se aplica a todo el espacio muestral. Cuando el caso de uso implica múltiples predictores, que interaccionan entre ellos de forma compleja y no lineal, es muy difícil encontrar un único modelo global que sea capaz de reflejar la relación entre las variables. Los métodos de _machine learning_ basados en árboles engloban a un conjunto de técnicas supervisadas no paramétricas que consiguen segmentar el espacio de los predictores en regiones simples, dentro de las cuales es más sencillo manejar las interacciones. Es esta característica la que les proporciona gran parte de su potencial.

### **1.1 Consideraciones**

* Es sencillo, intuitivo y fácil de entender.

* Los resultados pueden ser iguales a los de una regresión con muchas interacciones, el problema es que en una regresión tiene que definirse exante las interacciones. En arboles las interacciones las definen los datos.

* Los arboles tienden a hacer overfit.

* Son costosos computacionalmente.
 
* Si la relación entre la variable dependiente y los predictores es lineal, los arboles no pueden ser la mejor opción.

* No es necesario escalar las variables (a diferencia de Lasso)

### **1.2 Ventajas**

* Los árboles son fáciles de interpretar aun cuando las relaciones entre predictores son complejas.

* Los modelos basados en un solo árbol se pueden representar gráficamente aun cuando el número de predictores es mayor de 3.

* Los árboles pueden, en teoría, manejar tanto predictores numéricos como categóricos sin tener que crear variables dummy. 

* Al tratarse de métodos no paramétricos, no es necesario que se cumpla ningún tipo de distribución específica.

* Por lo general, requieren mucha menos limpieza y preprocesado de los datos en comparación con otros métodos de aprendizaje estadístico (por ejemplo, no requieren estandarización).

* No se ven muy influenciados por _outliers_ en comparación con las regresiones lineales.

* Si para alguna observación, el valor de un predictor no está disponible, a pesar de no poder llegar a ningún nodo terminal, se puede conseguir una predicción empleando todas las observaciones que pertenecen al último nodo alcanzado. La precisión de la predicción se verá reducida pero al menos podrá obtenerse.

* Son muy útiles en la exploración de datos, permiten identificar de forma rápida y eficiente las variables (predictores) más importantes.

* Son capaces de seleccionar predictores de forma automática.

* Pueden aplicarse a problemas de regresión y clasificación.

### **1.3 Desventajas**

* La capacidad predictiva de los modelos basados en un único árbol es bastante inferior a la conseguida con otros modelos. Esto es debido a su tendencia al _overfitting_ y alta varianza. Sin embargo, existen técnicas más complejas que, haciendo uso de la combinación de múltiples árboles (bagging, random forest, boosting), consiguen mejorar en gran medida este problema.

* Son sensibles a datos de entrenamiento desbalanceados (una de las clases domina sobre las demás).

* Cuando tratan con predictores continuos, pierden parte de su información al categorizarlos en el momento de la división de los nodos.

* La creación de las ramificaciones de los árboles se consigue mediante el algoritmo de _recursive binary splitting_. Este algoritmo identifica y evalúa las posibles divisiones de cada predictor acorde a una determinada medida (RSS, Gini, entropía…). Los predictores continuos tienen mayor probabilidad de contener, solo por azar, algún punto de corte óptimo, por lo que suelen verse favorecidos en la creación de los árboles.

* No son capaces de extrapolar fuera del rango de los predictores observado en los datos de entrenamiento.

<!---------------------->
## **[2.] Aplicación**

### **2.0 Configuración inicial** 

Para replicar las secciones de esta clase, primero debe descargar el siguiente [proyecto de R](https://gitlab.com/lectures-r/big-data-real-state-202301/clase-08/-/archive/main/clase-08-main.zip?path=clase-08) y abrir el archivo `clase-08.Rproj`. 

#### **2.0.0 Instalar/llamar las librerías de la clase**

```{r , eval=T}
require(pacman) 
p_load(tidyverse,rio,janitor,
       caret, glmnet, elasticnet,ranger,
       yardstick, ## metric_set
       tune, ## tune_grid, select_best
       rsample, ## vfold_cv
       dials, ## grid_regular
       rattle, ## fancyRpartPlot
       parsnip,tree,rpart,rpart.plot) ## trees
```

#### **2.0.1 fijar semilla**
```{r , eval=T}
set.seed(12345)
```

#### **2.0.2 leer datos**
```{r , eval=T}
db <- import("input/data_regresiones.rds") %>% na.omit() %>% select(-id) %>% clean_names()
```

#### **2.0.3 prepare db**
```{r , eval=T}
### tibble to matrix
df <- model.matrix(~.+ I(surface_total^2) + I(dist_cbd^2) -1 -price , db) %>% as_tibble() %>% clean_names()

df$price <- db$price/1000000 %>% unlist()

### partir la muestra
sample_test <- sample(x = nrow(df) , size = nrow(df)*.3)

test <- df[sample_test,]

train <- df[-sample_test,]
```

<!---------------->
### **2.1 Lasso**

```{r , eval=T}
## predictores
x <- train %>% select(-price) %>% as.matrix()
y <- train$price

## estimations
lasso <- glmnet(x=x , y=y , alpha=1) ## (alpha = 1 para lasso)
dim(coef(lasso)) ## Número de variables y modelos
plot(x=lasso, xvar="lambda" , label=TRUE) ## plot

## mejor lambda (CV)
lasso_cv <- cv.glmnet(x = x , y = y , alpha=1 , nfolds = 10)
min_lambda = lasso_cv$lambda.min

## hacer predicción con mejor lambda
coef(lasso)[,which(lasso$lambda==min_lambda)]

test$price_lasso_cv <- predict(object=lasso , s=min_lambda , newx=select(test,-price) %>% as.matrix()) %>% unlist()

rmse_lasso = sqrt(with(test,mean((price-price_lasso_cv)^2)))

rmse_lasso
```

<!---------------->
### **2.2 Arboles**
```{r , eval=T}
## definir modelo
set_1 <- decision_tree() %>% set_engine("rpart") %>% set_mode("regression")

## ajustar primer modelo: 
model_1 <- fit(object = set_1 , formula = price ~ . , data = train)

model_1

fancyRpartPlot(model = model_1$fit)

## Importancia de las variables
import_1 <- varImp(model_1$fit) 
import_1

## reescalar los pesos
import_1 <- import_1 %>% as.data.frame() %>%
            rownames_to_column(var = "var") %>%
            mutate(peso = Overall/sum(Overall)) %>%
            filter(peso > 0)

ggplot(import_1, aes(x=peso, y=reorder(var, peso))) +
geom_bar(stat="identity") + theme_test()

## predicción
test$price_tree <- predict(object=model_1 , new_data=test) %>% unlist()

rmse_tree = sqrt(with(test,mean((price-price_tree)^2)))

rmse_tree
```

<!---------------->
### **2.3 Arboles: Tune**
```{r , eval=T}
## definir modelo
set_2 <- decision_tree(cost_complexity = tune(), ## 
                       tree_depth = tune() , ## profundidad
                       min_n = tune() ## minima cantidad de observaciones por hoja
                       ) %>%  
         set_engine("rpart") %>% set_mode("regression")

## crear grilla con hiperparametos a probar
grid_regular(cost_complexity(), 
             min_n(),
             tree_depth(), 
             levels = 4)

tree_grid <- crossing(cost_complexity = c(0.0001), 
                      min_n = c(2,14,27),
                      tree_depth = c(2,4,8))

## Definimos CV
folds <- vfold_cv(data=train , v=5) ## resamples

## ajustar segundo modelo: 
model_2 <- tune_grid(set_2, price ~ .,
                     resamples = folds,
                     grid = tree_grid,
                     metrics = metric_set(rmse)
)

## ver los hiperparámetros
collect_metrics(model_2)

## visualizar los modelos
autoplot(model_2) + theme_test()

## best model
select_best(model_2)

best_model_2 <- finalize_model(set_2, select_best(model_2))

## Entrenamos el mejor modelo
model_2_fit <- fit(object = best_model_2 , formula = price ~ . , data = train)

## cambia la importancia
varImp(model_2_fit$fit) %>% as.data.frame() %>%
rownames_to_column(var = "var") %>%
mutate(peso = Overall/sum(Overall)) %>%
filter(peso > 0) %>%
ggplot(., aes(x=peso, y=reorder(var, peso))) +
geom_bar(stat="identity") + theme_test()

## predicción
test$price_tree_tn <- predict(object=model_2_fit , new_data=test) %>% unlist()

rmse_tree_tn = sqrt(with(test,mean((price-price_tree_tn)^2)))

rmse_lasso
rmse_tree
rmse_tree_tn
```

<!---------------------->
## **[3.] Random Forests**

```{r , eval=T}
### 3.1 Solucion: Random Forests preserva bajo sesgo y reducen varianza
# En lugar de entrenar un arbol, se entrenan varios arboles.
# Pero no es suficiente, porque si se sigue entrenando los arboles usando el mismo
# set de datos, se sigue teniendo el problema de varianza alta.
# Entonces cada arbol se entrena con un subset de datos aleatorio diferente.
# Y se obtiene un promedio de todos los arboles.

### 2.2 Algoritmo de Random Forests con Bagging or Boostrap Agregation
## 1. Fijar el numero de arboles
## 2. Crear el subset de entrenamiento de cada arbol
###   Bootstrapping: Muestreo aleatorio con reemplazo
## 3. Seleccionar el numero de predictores
###   Se eligen aleatoriamente m predictores ; m = p^1/2 
###   donde p = numero de predictores en el dataset
###   Nota: Mencionar la importancia de la aleatoriedad en la eleccion de los predictores
### 4. Obtener la predicci

## modelo 1
rf_1 <- train(price ~ .,
              data = train[1:1000,], 
              method = "ranger", ## ramdon forest
              trControl = trainControl(number=5 , method="repeatedcv"))
rf_1

## plot
plot(rf_1)
```

```{r , eval=F}
### 3.3 Tunear un bosque
## 1. Fijar numero de predictores y criterio de parada y elegir numero de arboles
## 2. Fijar numero de arboles y criterio de parada y elegir el numero de predictores
## 3. Fijar numero de predictores y numero de arboles y elegir el criterio de parada

## Creamos una grilla para tunear el random forest
tunegrid_rf <- expand.grid(mtry = c(2,4,8), 
                           min.node.size = c(2,5,10),
                           splitrule = "variance")

## ranger me permite optimizar mas hiperparametros
## fit model
rf_2 <- train(price ~ .,
              data = train[1:1000,], 
              method = "ranger", 
              tuneGrid = tunegrid_rf)
rf_2

# plot
plot(rf_2)
```

```{r , eval=T}
## Ahora vamos a ver como cambian los resultados cuando cambio el número de árboles
resultados <- tibble()
for (t in seq(1, 100, 10)) {
     modelo_i <- ranger(price ~ ., 
                        data = train, 
                        num.trees = t,
                        mtry = 8, 
                        min.node.size = 10,
                        splitrule = "variance")
     
     rmse_in = sqrt(modelo_i[["prediction.error"]])
     test$price_rf_i <- predict(modelo_i , data=test)$predictions
     rmse_out <- sqrt(with(test,mean((price-price_rf_i)^2)))
     
     result_i <- tibble(arboles = t,rmse_in = rmse_in , rmse_out = rmse_out)
     resultados <- rbind(resultados, result_i)
}
resultados

ggplot() +
geom_line(data=resultados , aes(x=arboles , y=rmse_out) , col="red") + 
geom_line(data=resultados , aes(x=arboles , y=rmse_in) , col="blue") + 
geom_point() + theme_bw() +
labs(x = "Número de árboles", y = "RMSE",
     title = "Variación en el desempeño del Random Forest para diferente número de árboles")
```

